<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3">
    <title>Coto Neptuno</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom-themes.css') }}">
    <link rel="stylesheet" href="https://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="{{ asset('css/jquery.fileupload.css')}}">
    <link rel="stylesheet" href="{{ asset('css/jquery.fileupload-ui.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
</head>
<body>
    <div class="page-wrapper chiller-theme sidebar-bg toggled">
        <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
            <i class="fas fa-bars"></i>
        </a>
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content">
                <div class="sidebar-brand">
                    <a href="#">Coto Neptuno</a>
                    <div id="close-sidebar">
                        <i class="fas fa-times"></i>
                    </div>
                </div>
                <div class="sidebar-header">
                    <div class="user-pic">
                        <img class="img-responsive img-rounded" src="../../img/logo.png" alt="" width="40" height="50">
                    </div>
                    <div class="user-info">
                        <span class="user-name">
                            {{ Auth::user()->username }}
                        </span>
                        <span class="user-role">Administrador</span>
                    </div>
                </div>
                <!-- sidebar-search  -->
                <div class="sidebar-menu">
                    <ul>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fas fa-building"></i>
                                <span>Condominios</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href=""><i class="fas fa-home"></i> Gestionar UP</a>
                                    </li>
                                    <li>
                                        <a href=""><i class="fas fa-inbox"></i> Mensajería</a>
                                    </li>
                                    <li>
                                        <a href="{{route('complaints.show')}}"><i class="fas fa-user-check"></i> Quejas y Sugerencias</a>
                                    </li>
                                    <li>
                                        <a href="{{route('announcements.show')}}"><i class="fas fa-bullhorn"></i> Anuncios</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fab fa-jenkins"></i>
                                <span>Administración</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#"><i class="fas fa-users"></i> Usuarios</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-balance-scale"></i> Saldos</a>
                                    </li>
                                    <li>
                                        <a href="#"> <i class="fas fa-hand-holding-usd"></i> Tabla de Cobranza</a>
                                    </li>
                                    <li>
                                        <a href="#"> <i class="far fa-address-book"></i> Directorio de Habitantes</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fas fa-money-bill-alt"></i>
                                <span>Finanzas</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#"><i class="fas fa-arrow-left"></i> Ingresos</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-arrow-right"></i> Egresos</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-dollar-sign"></i> Cuotas</a>
                                    </li>
                                    <li>
                                        <a href="#">Cuotas por Servicio</a>
                                    </li>
                                    <li>
                                        <a href="#">Recargos</a>
                                    </li>
                                    <li>
                                        <a href="#">Condonaciones</a>
                                    </li>
                                    <li><a href="#">Morosos</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fas fa-file-alt"></i>
                                <span>Reportes</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#">Resumen General</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-chart-line"></i> Reporte Mensual</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-chart-bar"></i> Reporte Anual</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-chart-pie"></i> Reporte de Ingresos</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-chart-area"></i> Reporte Egresos</a>
                                    </li>
                                    <li>
                                        <a href="#">Reporte de Quejas</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="#">
                                <i class="fas fa-cogs"></i>
                                <span>Configuración</span>
                            </a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li>
                                        <a href="#"><i class="fas fa-info-circle"></i> Información Administrador</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-file-invoice"></i> Archivos Generales</a>
                                    </li>
                                    <li>
                                        <a href="#"> <i class="fas fa-at"></i> Email de Bienvenida</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="fas fa-user-secret"></i> Gestionar Administradores</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <li>
                            <a href="{{ route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="fas fa-sign-out-alt"></i> Cerrar Sesión
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- sidebar-menu  -->
            </div>
            <!-- sidebar-content  -->
        </nav>
        <!-- sidebar-wrapper  -->
        <main class="page-content">
            @yield('content')
        </main>
        <!-- page-content" -->
    </div>
    <!-- page-wrapper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <script src="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</body>

<script src="{{ asset('js/custom.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>
</html>