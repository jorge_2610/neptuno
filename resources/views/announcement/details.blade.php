@extends('welcome')

@section('title', 'Announcements')
@section('content')
<style type="text/css">
	.btn-outline-secondary{color: #092f50}
	.btn-outline-secondary:hover{background-color: #092f50}
</style>
<div class="container">
	<div class="text-center">
		<a href="{{ route('announcements.show') }}">
			<button type="button" class="btn btn-responsive btn-outline-secondary mb-2">
				<i class="fa fa-arrow-circle-left"></i> Regresar
			</button>
		</a>
	</div>
	<div class="card">
	    <div class="card-body">
	        <div class="row">
        	    <div class="col-md-10">
        	       	<div class="clearfix">
	        	       	<div><a style="color: steelblue"><strong> {{$admin->username}}</strong> - {{$announcement->date}}</a></div>
	        	       		<span><strong>Categoría: </strong>{{$category -> name}}</span>
	        	        </div>
	        	        <div style="font-size: 22px; color: #b0162c;">{{$announcement-> title}}</div>
	        	        <div style="font-size: 17px;" class="mt-2">{{$announcement-> content}}</div>
        	    </div>
	        </div>
	        <div id="carouselExampleIndicators" class="carousel slide mb-2" data-ride="carousel">
			  	<div class="carousel-inner">
			    	@forelse ($photos as $photo)
			    		<div @if ($loop->first) class="carousel-item active" @else class="carousel-item" @endif>
			     			<img class="d-block w-100" src="{{url('/images/')}}/{{$photo-> name}}">
			    		</div>
				    @empty
					@endforelse
			  	</div>
			  	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			    	<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    	<span class="sr-only">Previous</span>
			  	</a>
			  	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
			    	<span class="sr-only">Next</span>
			  	</a>
			</div>
			<div class="mb-1">
				<strong>Fecha de Expiración : </strong><span style="color:steelblue;">{{$announcement-> expirationDate}}</span>
			</div>
			<div class="text-center">
				@forelse ($files as $file)
		    		<div style="float: left;padding: 7px;display: inline-block;" class="text-center">
						<a href="{{ route('announcements.downloadFile', ['name' => $file-> name]) }}" target="_blank">
							<div style="background-image: url('../../img/{{substr($file->name,strpos($file->name,'.')+1)}}.png');height: 100px;width: 120px;background-position: center;background-size: contain;background-repeat: no-repeat;}">
							</div>
						  	<div style="width: 120px;"class="text-center">
						  		{{str_limit($file -> name, $limit = 30, $end = '...')}}
						  	</div>
						</a>
		    		</div>
			    @empty
				@endforelse
			</div>
        	@forelse ($comments as $comment)
        	<div class="card card-inner" style="clear: both;width: 100%!important">
        	    <div class="card-body">
        	        <div class="row">
                	    <div style="width: 80%;padding: 0px 15px 0px 15px;">
                	        <a style="color: steelblue"><strong>{{$comment-> username}}</strong></a><br>
                	        <p style="color: steelblue">{{$comment-> date}}</p>
                	    </div>
                	    @if($comment->userID == Auth::user()->id)
                	    <div style="width: 20%;">
        	        		<a style="float: right;"href="{{route('announcements.deleteComment', ['id' => $comment-> id]) }}">
		                        <button type="button" class="btn btn-responsive btn-danger">
		                            <i class ="fa fa-trash"></i>
		                        </button>
		                    </a>
                	    </div>
                	    @endif
                	    <div class="col-sm-12 col-xs-12">
                	    	<p>{{$comment->content}}</p>
                	    </div>
                	</div>
        	    </div>
            </div>
            @empty
			@endforelse
			<form id="fileupload" action="{{ url('comment') }}" method="post">
				{{ csrf_field() }}
				<div class="input-group mt-3">
		            <div class="input-group-prepend">
		                <span class="input-group-text" style="color: white;background: #092f50;" id="basic-addon1"><i class="fa fa-comments"></i></span>
		                <input type="" name="idAnnouncement" value="{{$announcement -> id}}" style="display: none;">
		            </div>
			 		<textarea class="form-control" required name="content" placeholder="Escribe un Comentario..."></textarea>
				 	<div class="input-group-append">
						<button class="btn btn-outline-secondary" type="submit"><i style="font-size: 25px;" class="fas fa-arrow-circle-right"></i></button>
					</div>
				</div>
			</form>
	    </div>
	</div>
</div>
@endsection