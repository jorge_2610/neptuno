@extends('welcome')

@section('content')

<!DOCTYPE HTML>
<html lang="en">
<head>
<style type="text/css">
	.container {width: 100% !important;}
    .input-group-text {color: white !important;background-color: #092f50 !important;}
    .btn-outline-secondary{color: #092f50}
    .btn-outline-secondary:hover{background-color: #092f50}
    ul{list-style:none}
    li{list-style:none}
    .img-with-text {
    text-align: justify;
    width: [width of img];
    }
</style>
</head>
<body>
<div class="container">
    <div class="text-center">
        <a href="{{ route('announcements.show') }}">
            <button type="button" class="btn btn-responsive btn-outline-secondary mb-1 mt-1">
                <i class="fa fa-arrow-circle-left"></i> Regresar
            </button>
        </a>
    </div>
    <form id="fileupload" action="{{ url('editAnn') }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
        <input type="" name="id" value="{{$announcement -> id}}" style="display: none;">
		<div class="input-group mb-3" style="margin-top: 10px;">
		  <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1">@</span>
          </div>
		  <input type="text" class="form-control" required placeholder="Título" name="title" aria-label="Título" aria-describedby="basic-addon1" value="{{$announcement -> title}}">
		</div>
		<div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">Contenido</span>
            </div>
		 	<textarea class="form-control" required name="content" aria-label="Textarea"> {{$announcement -> content}} </textarea>
		</div>
		<label style="color:black !important" >Fecha de Expiración del Anuncio: </label>
		<div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1"><i class="fa fa-calendar"></i></span>
            </div>
			<input type="date" name="expirationDate" value="{{$announcement-> expirationDate }}" required max="3000-12-31" min="{{ date('Y-m-d') }}" class="form-control">
		</div>

        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <label class="input-group-text" for="inputGroupSelect01">Categoría</label>
            </div>
            <select class="custom-select" placeholder="Selecciona una Categoría" name="categoryID" required>
                @forelse ($announcement_categories as $category)
                    <option name="categoryID" value="{{$category-> id}}" @if ($category->id == $announcement->categoryID) selected @else @endif>{{$category->name}}</option>
                @empty
                @endforelse
            </select>
        </div>
        <div class="fileupload-buttonbar mt-2 text-center">
            <div class="col-xs-12 mt-2">
                <span class="fileinput-button input-group-text mt-2">
                    <i class="fa fa-camera"></i>
                    <span> Añadir Imágenes al Anuncio</span>
                    <input  accept=".jpg, .jpeg, .png"  id="file-input" name="files[]" type="file" multiple="multiple" />
                </span>
            </div>
            <div class="container text-center mb-2"id="preview"></div>
        </div>
        <div class="text-center">
            <a href="#" class="btn" style="color: white;background: #092f50;" id="fileSelect"><i class="fa fa-file"></i> Añadir Archivos al Anuncio</a> 
            <input accept=".xlsx,.xls,.doc,.docx,.pdf" onchange="handleFiles(this.files)" id="fileElem" multiple type="file" class="mb-1 mt-1" name="documents[]" style="display: none;" />
            <div class="text-center">
                <output id="list"></output>
            </div>
        </div>
        <div class="container text-center" style="margin-top: 5px;">
            <button type="submit" class="btn btn-success mb-2"><i class="fas fa-save"></i> Guardar Anuncio</button>
        </div>
    </form>
    <hr>
    <div class="text-center" style="clear: both">
        @if(count($photos) > 0)
        <div style="color: #b0162c;font-size: 18px;">Fotos del Anuncio</div>
        @forelse ($photos as $photo)
            <div style="padding: 7px;display: inline-block;" class="text-center img-with-text">
                <div style="background-image: url('/images/{{$photo-> name}}');background-repeat: no-repeat;background-size: cover;background-position: center; height: 100px; width: 190px;">
                    <a style="position: relative;left: 75px;"href="{{ route('announcements.deletePhoto', ['name' => $photo-> id]) }}">
                        <button type="button" class="btn btn-responsive btn-danger">
                            <i class ="fa fa-trash"></i>
                        </button>
                    </a>
                </div>
            </div>
        @empty
        @endforelse
        @endif
    </div>
    <div class="text-center" style="clear: both">
         @if(count($files) > 0)
        <div style="color: #b0162c;font-size: 18px;">Archivos del Anuncio</div>
        @forelse ($files as $file)
            <div style="padding: 7px;display: inline-block;" class="text-center img-with-text">
                    <div style="background-image: url('../../img/{{substr($file->name,strpos($file->name,'.')+1)}}.png');height: 100px;width: 120px;background-position: center;background-size: contain;background-repeat: no-repeat;}">
                        <a style="position: relative;left: 40px;"href="{{ route('announcements.deleteFile',['id' => $file-> id]) }}">
                            <button type="button" class="btn btn-responsive btn-danger">
                                <i class ="fa fa-trash"></i>
                            </button>
                        </a>
                    </div>
                    <div style="width: 120px;"class="text-center">
                        {{str_limit($file -> name, $limit = 12, $end = '...')}}
                    </div>
            </div>
        @empty
        @endforelse
        @endif
    </div>
</div>
</script>
<script type="text/javascript">
    function previewImages() {
        document.getElementById('preview').innerHTML = "";
        var preview = document.querySelector('#preview');
        if (this.files) {
            [].forEach.call(this.files, readAndPreview);
        }
        function readAndPreview(file) {
            if (!/\.(jpe?g|png|gif)$/i.test(file.name)) {
                return alert(file.name + " no es una Imagen");
            }
            var reader = new FileReader();
            reader.addEventListener("load", function() {
            var image = new Image();
            image.height = 100;
            image.title  = file.name;
            image.src    = this.result;
            image.style['padding'] = '2px';
            preview.appendChild(image);
            }, false);
            reader.readAsDataURL(file);
        }
    }
    document.querySelector('#file-input').addEventListener("change", previewImages, false);

    window.URL = window.URL || window.webkitURL;

    var fileSelect = document.getElementById("fileSelect"),
        fileElem = document.getElementById("fileElem"),
        fileList = document.getElementById("fileList");

    fileSelect.addEventListener("click", function (e) {
      if (fileElem) {
        fileElem.click();
      }
      e.preventDefault(); // prevent navigation to "#"
    }, false);

    function handleFiles(files) {
        var output = [];
        for (var i = 0, f; f = files[i]; i++) {
            console.log(escape(f.name).split('.').pop());
            output.push('<img class="img-responsive img-rounded" src="../../img/',escape(f.name).split('.').pop(),'.png" alt="File" width="40" height="50"><li><strong>', escape(f.name), '</strong>','</li>');
        }
    document.getElementById('list').innerHTML = '<ul>' + output.join('') + '</ul>';
    }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')
</body>
</html>
@endsection