@extends('welcome')

@section('title', 'Announcements')
@section('content')
    <link class="rounded-list" href="{{ asset('css/announcements.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <style type="text/css">
        ul {margin-top: 5px;margin-bottom: -15px;}
        .page-item.active .page-link {z-index: 1;color: #fff;background-color: #092f50;border-color: #092f50;}
        .page-link{color: #092f50}
        .page-item {display: inline !important;text-align: center !important;}
        .contenedor ul {display:inline-flex;text-align: initial;margin-bottom: 5px;}
    </style>
    <div style="text-align: center;">
        <a href="{{route('announcements.create')}}">
            <button type="button" class="btn btn-responsive btn-outline-success" style="margin: 5px 5px 5px 5px;color: ">
                <i class ="fa fa-plus"></i> Agregar Anuncio
            </button>
        </a>
    </div>
    <div class="actu-list">
        @forelse ($announcements as $announcement)
        <div>
            <div class="article" style="overflow: hidden;">
                    @if ($announcement -> getImageAttribute())
                    @forelse ($announcement -> getImageAttribute() as $test)
                        <div class="article-photo horizontal" style="background-image: url('/images/{{$test}}');background-repeat: no-repeat;background-size: cover;background-position: center;">
                    @empty
                    @endforelse
                    @else
                        <div class="article-photo horizontal" style="background-image: url('https://www.chartboost.com/wp-content/uploads/2015/05/productlaunch_GIF_06.gif');background-repeat: no-repeat;background-size: cover;background-position: center;">
                    @endif
                    <h1 class="article-title hasSubtitle">{{$announcement -> title }}</h1>
                    <h4 class="article-title" style="font-size: 20px !important;">{{$announcement->date}}</h4>
                    <div class="shape-img">
                        <a href="{{ route('announcements.edit', ['id' => $announcement-> id]) }}">
                            <button type="button" class="btn-responsive btn btn-outline-warning" style="margin: 10px 5px 10px 10px;">
                                <i class ="fa fa-edit"></i> Editar
                            </button>
                        </a>
                        <a href="{{ route('announcements.delete', ['id' => $announcement-> id]) }}">
                            <button type="button" class="btn btn-responsive btn-outline-danger" style="margin: 10px 5px 10px 0px;">
                                <i class ="fa fa-trash"></i> Eliminar
                            </button>
                        </a>
                        <a href="{{ route('announcements.details', ['id' => $announcement-> id]) }}">
                            <button type="button" class="btn btn-responsive btn-outline-primary"style="margin: 10px 0px 10px 0px;">
                                <i class ="fa fa-eye"></i> Ver detalles
                            </button>
                        </a>
                        <div class="inline" style="position: relative;top: 10px;left: 10px;letter-spacing: 10px;"></div>
                    </div>
                    <div class="type white" style="background-color: rgba(228, 20, 20, 0.4);color: white;"><i class="fas fa-bullhorn megaphone"></i></div>
                    <div class="article-subtitle content-text line-clamp">{{str_limit($announcement-> content, $limit = 169, $end ='...')}} </div>
                </div>
            </div>
        </div>
        @empty
        <div class='fullscreenDiv'>
            <div class="center">No hay Anuncios Publicados</div>
        </div>​
        @endforelse
        @if($announcements->count() < 1)
            <script type="text/javascript">
                 window.location = "/announcements";
            </script>
        @endif
        @if($announcements->total() > $announcements->perPage())
            <div class="contenedor" style="width: 100%;text-align: center;">
                {!! $announcements->links('vendor.pagination.bootstrap-4') !!}﻿
            </div>
        @endif
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @include('sweet::alert')
@endsection