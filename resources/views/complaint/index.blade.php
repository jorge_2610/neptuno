@extends('welcome')

@section('title', 'Announcements')
@section('content')
    <style type="text/css">
    .btn-outline-secondary{color: #092f50}
    .btn-outline-secondary:hover{background-color: #092f50}
    .filterDiv { display: none;}
    .show {display: block;}
    </style>
    <link class="rounded-list" href="{{ asset('css/complaints.css') }}" rel="stylesheet">
    <link class="rounded-list" href="{{ asset('css/announcements.css') }}" rel="stylesheet">
    <div class="container mb-3">
        <div id="myBtnContainer" class="text-center mb-2 mt-1">
            <button class="btn btn-responsive btn-outline-primary active" onclick="filterSelection('all')">
                Todas
            </button>
            <button class="btn btn-responsive btn-outline-danger" onclick="filterSelection('Nueva')">
                <i class="fas fa-info-circle"></i> Nuevas
            </button>
            <button class="btn btn-responsive btn-outline-warning" onclick="filterSelection('Procesando')">
                <i class="fa fa-spinner"></i> Procesando
            </button>
            <button class="btn btn-responsive btn-outline-success" onclick="filterSelection('Resuelta')">
                <i class="fa fa-check"></i> Resueltas
            </button>
        </div>
        @forelse ($complaints as $complaint)
            <div class="card card-inner filterDiv  {{$complaint->status}}" style="clear: both;width: 100%!important">
                <div class="card-body {{$complaint->status}}">
                    <div class="row">
                        <div style="width: 85%;padding: 0px 15px 0px 15px;">
                            <a style="color: steelblue"><strong>{{\App\User::find($complaint->userID)->username}}</strong></a> -
                            <a style="color: steelblue">{{$complaint-> date}}</a><br>
                            <p style="color: black"><strong>Estatus: </strong>{{$complaint-> status}}</p>
                        </div>
                        <div style="width: 15%;" class="text-center">
                            <a style=""href="">
                                <button type="button" class="btn btn-warning">
                                    <i class ="fa fa-eye"></i>
                                </button>
                            </a>
                        </div>
                        <div class="col-sm-12 col-xs-12">
                            <p>{{$complaint->content}}</p>
                        </div>
                    </div>
                </div>
            </div>
        @empty
        @endforelse
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @include('sweet::alert')
    <script>
    filterSelection("all")
    function filterSelection(c) {
        var x, i;
        x = document.getElementsByClassName("filterDiv");
        if (c == "all") c = "";
        for (i = 0; i < x.length; i++) {
            RemoveClass(x[i], "show");
            if (x[i].className.indexOf(c) > -1) 
                AddClass(x[i], "show");
        }
    }

    function AddClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            if (arr1.indexOf(arr2[i]) == -1) {
                element.className += " " + arr2[i];
            }
        }
    }

    function RemoveClass(element, name) {
        var i, arr1, arr2;
        arr1 = element.className.split(" ");
        arr2 = name.split(" ");
        for (i = 0; i < arr2.length; i++) {
            while (arr1.indexOf(arr2[i]) > -1) {
                arr1.splice(arr1.indexOf(arr2[i]), 1);     
            }
        }
        element.className = arr1.join(" ");
    }

    var btnContainer = document.getElementById("myBtnContainer");
    var btns = btnContainer.getElementsByClassName("btn");
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function(){
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
    }
</script>
@endsection