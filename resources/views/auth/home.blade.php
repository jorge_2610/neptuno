@if(Auth::user())
    <script>window.location = "/announcements";</script>
@else
    <script>window.location = "/login";</script>
@endif