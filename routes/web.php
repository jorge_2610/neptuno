<?php

Auth::routes();

Route::get('/createAnnouncement', 'AnnouncementController@create')->middleware('auth')->name('announcements.create');
Route::get('/home', 'AnnouncementController@home');
Route::get('/getAnnouncement/{id}','AnnouncementController@get')->where('id','[0-9]+')->middleware('auth')->name('announcements.details');
Route::get('/editAnnouncement/{id}', 'AnnouncementController@edit')->where('id', '[0-9]+')->middleware('auth')->name('announcements.edit');
Route::get('/deleteAnnouncement/{id}', 'AnnouncementController@delete')->where('id', '[0-9]+')->middleware('auth')->name('announcements.delete');
Route::get('/deletePhoto/{id}', 'AnnouncementController@deletePhoto')->middleware('auth')->name('announcements.deletePhoto');
Route::get('/deleteComment/{id}', 'AnnouncementController@deleteComment')->middleware('auth')->name('announcements.deleteComment');
Route::get('/deleteFile/{id}', 'AnnouncementController@deleteFile')->middleware('auth')->name('announcements.deleteFile');
Route::get('/downloadFile/{name}', 'AnnouncementController@downloadFile')->middleware('auth')->name('announcements.downloadFile');
Route::get('/announcements', 'AnnouncementController@getAll')->middleware('auth')->name('announcements.show');
Route::post('/createAnn', 'AnnouncementController@createAnnouncement')->middleware('auth');
Route::post('/editAnn', 'AnnouncementController@editAnnouncement')->middleware('auth');
Route::post('/addImagesAnn', 'AnnouncementController@addImages')->middleware('auth');
Route::post('/comment', 'AnnouncementController@comment')->middleware('auth');

Route::get('/complaints', 'ComplaintController@getAll')->middleware('auth')->name('complaints.show');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/','AnnouncementController@redirect');
});