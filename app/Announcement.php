<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{

	protected $appends = ['image'];

    public function getImageAttribute()
	{
		if(!empty($this-> image)){
	     return $this-> image;
		}
		else {
			return null;
		}
	}

	public function setImageAttribute($value)
	{
	     $this-> image = $value;
	}
}
