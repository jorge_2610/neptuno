<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Announcement;
use App\AnnouncementsCategory;
use App\AnnouncementsImage;
use App\Comment;
use App\Image;
use App\File;
use App\AnnouncementsFile;
use App\User;
use Carbon\Carbon;
use \stdClass;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AnnouncementController extends Controller
{
	public $idAnnouncement;

    public function getAll(){
    	$announcements = \App\Announcement::orderBy('date','DESC')->paginate(10);
    	foreach ($announcements as $ann) {
    		$photo = DB::table('announcements_images')
			->join('images','images.id','=','announcements_images.imageID','inner',false)
			->select('images.name')->where('announcementID',$ann->id)->first();
			if($photo){
				$ann->setImageAttribute($photo);
			}
    	}
		return view('announcement.index', compact('announcements'));
    }

    public function redirect(){
    	return redirect('/announcements');
    }

    public function deletePhoto($id){
    	DB::table('announcements_images')->where('imageID','=',$id)->delete();
    	alert()->success('Imagen Eliminada Correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }

    public function deleteComment($id){
    	DB::table('comments')->where('id','=',$id)->delete();
    	alert()->success('Comentario Eliminada Correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }

    public function deleteFile($id){
    	DB::table('announcements_files')->where('fileID','=',$id)->delete();
    	alert()->success('Archivo Eliminado Correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }

    public function home (){
        if (Auth::check()) {
            return redirect('/announcements');
        }
        else {
            return redirect('/login');
        }
    }

    public function index(){
    	$announcements = Announcement::all();
		return view('announcement.index', compact('announcements'));
    }

    public function delete($id){
    	DB::table('comments')->where('announcementID','=',$id)->delete();
    	DB::table('announcements_images')->where('announcementID','=',$id)->delete();
    	DB::table('announcements_files')->where('announcementID','=',$id)->delete();
        DB::table('announcements')->where('id', '=', $id)->delete();
        alert()->success('Anuncio Eliminado correctamente','Éxito.')->autoclose(3000);
        return redirect()->back();
    }

    public function create (){
    	$announcement_categories = AnnouncementsCategory::all();
    	return view('announcement.create',compact('announcement_categories'));
    }


    public function downloadFile($name){
    	$file_path = public_path('documents/'.$name);
    	return response()->download($file_path);
    }

    public function createAnnouncement(Request $request)
	{
		$announcement = new Announcement();
		$announcement->title = $request->input("title");
		$announcement->content = $request->input("content");
		$announcement->expirationDate = $request->input("expirationDate");
		$announcement->categoryID = $request->input("categoryID");
		$announcement->userID = Auth::user()->id;
		$announcement->date = Carbon::now()->toDateTimeString();
		$announcement->save();

		$id = $announcement->id;

	    if ($request->hasFile('files')) {
	        foreach($request->file('files') as $file){
	            $filenamewithextension = $file->getClientOriginalName();
	            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
	            $extension = $file->getClientOriginalExtension();
	            $filenametostore = $filename.'.'.$extension;
	            $img_f = $file->getPathName();
	            $val = md5_file($img_f);
	            $imagen = $val.'.'.$extension;
				Storage::disk('public')->put($imagen, fopen($file, 'r+'));
				$image = DB::table('images')->where('name', '=', $imagen)->first();
				if(!$image){
            		$image = new Image();
            		$image->name = $imagen;
					$image->save();
				}
            	$annImage = new AnnouncementsImage();
            	$annImage-> announcementID = $id;
            	$idImage = $image -> id;
            	$annImage-> imageID = $idImage;
            	$annImage->save();
	        }
	    }

	    if ($request->hasFile('documents')) {
	    	foreach($request->file('documents') as $file){
	            $filenamewithextension = $file->getClientOriginalName();
	            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
	            $extension = $file->getClientOriginalExtension();
	            $document = $filename.'.'.$extension;
				Storage::disk('files')->put($document, fopen($file, 'r+'));
				$doc = DB::table('files')->where('name', '=', $document)->first();
				if(!$doc){
            		$doc = new File();
            		$doc->name = $document;
					$doc->save();
				}
            	$annFile = new AnnouncementsFile();
            	$annFile-> announcementID = $id;
            	$idFile = $doc -> id;
            	$annFile-> fileID = $idFile;
            	$annFile->save();
	        }
	    }

	    alert()->success('Anuncio Agregado correctamente','Éxito.')->autoclose(3000);

	    return redirect('announcements');
	}


	public function editAnnouncement(Request $request)
	{
		$id = $request->input("id");
		$announcement = Announcement::find($id);
		$announcement->title = $request->input("title");
		$announcement->content = $request->input("content");
		$announcement->expirationDate = $request->input("expirationDate");
		$announcement->categoryID = $request->input("categoryID");
		$announcement->userID = Auth::user()->id;
		$announcement->save();

		if ($request->hasFile('files')) {
	        foreach($request->file('files') as $file){
	            $filenamewithextension = $file->getClientOriginalName();
	            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
	            $extension = $file->getClientOriginalExtension();
	            $filenametostore = $filename.'.'.$extension;
	            $img_f = $file->getPathName();
	            $val = md5_file($img_f);
	            $imagen = $val.'.'.$extension;
				Storage::disk('public')->put($imagen, fopen($file, 'r+'));
				$image = DB::table('images')->where('name', '=', $imagen)->first();
				if(!$image){
            		$image = new Image();
            		$image->name = $imagen;
					$image->save();
				}
            	$annImage = new AnnouncementsImage();
            	$annImage-> announcementID = $id;
            	$annImage-> $id;
            	$idImage = $image -> id;
            	$annImage-> imageID = $idImage;
            	$annImage->save();
	        }
	    }
	    if ($request->hasFile('documents')) {
	    	foreach($request->file('documents') as $file){
	            $filenamewithextension = $file->getClientOriginalName();
	            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
	            $extension = $file->getClientOriginalExtension();
	            $document = $filename.'.'.$extension;
				Storage::disk('files')->put($document, fopen($file, 'r+'));
				$doc = DB::table('files')->where('name', '=', $document)->first();
				if(!$doc){
            		$doc = new File();
            		$doc->name = $document;
					$doc->save();
				}
            	$annFile = new AnnouncementsFile();
            	$annFile-> announcementID = $id;
            	$idFile = $doc -> id;
            	$annFile-> fileID = $idFile;
            	$annFile->save();
	        }
	    }

	    alert()->success('Anuncio Editado correctamente','Éxito.')->autoclose(3000);
	    return redirect()->back();
	}

	public function get($id){
		$announcement = Announcement::find($id);
		$admin = User::find($announcement->userID);
		$comments = DB::table('comments')
		->join('users', 'users.id', '=', 'comments.userID', 'inner', false)
		->select('comments.*', 'users.username')->where('announcementID',$id)->orderBy('date')->get();
		$photos = DB::table('announcements_images')
		->join('images','images.id','=','announcements_images.imageID','inner',false)
		->select('announcements_images.*','images.name')->where('announcementID',$id)->get();
		$files = DB::table('announcements_files')
		->join('files','files.id','=','announcements_files.fileID','inner',false)
		->select('announcements_files.*','files.name')->where('announcementID',$id)->get();
		$category = AnnouncementsCategory::find($announcement->categoryID);
		return view ('announcement.details',compact('announcement','comments','admin','photos','files','category'));
	}


	public function edit($id){
		$announcement = Announcement::find($id);
		$admin = User::find($announcement->userID);
		$comments = DB::table('comments')
		->join('users', 'users.id', '=', 'comments.userID', 'inner', false)
		->select('comments.*', 'users.username')->where('announcementID',$id)->get();
		$photos = DB::table('announcements_images')
		->join('images','images.id','=','announcements_images.imageID','inner',false)
		->select('announcements_images.*','images.*')->where('announcementID',$id)->get();
		$files = DB::table('announcements_files')
		->join('files','files.id','=','announcements_files.fileID','inner',false)
		->select('announcements_files.*','files.*')->where('announcementID',$id)->get();
		$announcement_categories = AnnouncementsCategory::all();
		$category = AnnouncementsCategory::find($announcement->categoryID);
		return view ('announcement.edit',compact('announcement','comments','admin','photos','files','category','announcement_categories'));
	}

	public function addImages(Request $request)
	{
	    if ($request->hasFile('files')) {
	        foreach($request->file('files') as $file){
	            $filenamewithextension = $file->getClientOriginalName();
	            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
	            $extension = $file->getClientOriginalExtension();
	            $filenametostore = $filename.'.'.$extension;
	            $img_f = $file->getPathName();
	            $val = md5_file($img_f);
	            $imagen = $val.'.'.$extension;
				Storage::disk('public')->put($imagen, fopen($file, 'r+'));
				$image = DB::table('images')->where('name', '=', $imagen)->first();
				if(!$image){
            		$image = new Image();
            		$image->name = $imagen;
					$image->save();
				}
            	$annImage = new AnnouncementsImage();
            	$annImage-> announcementID = $request->input("idAnnouncement");
            	$idImage = $image -> id;
            	$annImage-> imageID = $idImage;
            	$annImage->save();
	        }
	    }
	    if ($request->hasFile('documents')) {
	    	foreach($request->file('documents') as $file){
	            $filenamewithextension = $file->getClientOriginalName();
	            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
	            $extension = $file->getClientOriginalExtension();
	            $document = $filename.'.'.$extension;
				Storage::disk('files')->put($document, fopen($file, 'r+'));
				$doc = DB::table('files')->where('name', '=', $document)->first();
				if(!$doc){
            		$doc = new File();
            		$doc->name = $document;
					$doc->save();
				}
            	$annFile = new AnnouncementsFile();
            	$annFile-> announcementID = $request->input("idAnnouncement");
            	$idFile = $doc -> id;
            	$annFile-> fileID = $idFile;
            	$annFile->save();
	        }
	    }

	    alert()->success('Archivos Guardados Correctamente','Éxito.')->autoclose(3000);

	    return redirect()->back();
	}

	public function comment(Request $request){
		$comment = new Comment();
		$comment->userID = Auth::user()->id;
		$comment->date = Carbon::now()->toDateTimeString();
		$comment->content = $request->input("content");
		$comment->announcementID = $request->input("idAnnouncement");
		$comment->save();
		return redirect()->back();
	}
}