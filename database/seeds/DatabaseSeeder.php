<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(AnnouncementCategorySeeder::class);
        $this->call(AnnouncementSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(ComplaintSeeder::class);
    }
}
