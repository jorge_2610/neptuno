<?php

use App\Announcement;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run(){
        $announcement = new Announcement();
        $announcement->content ='Este es un Anuncio de Prueba.';
        $announcement->title = 'Anuncio de Prueba';
        $announcement->date = '2018-09-21';
        $announcement->expirationDate = '2018-09-23';
        $announcement->categoryID=1;
        $announcement->userID=1;
        $announcement->save();


        $announcement = new Announcement();
        $announcement->content ='Se le informa a toda la comunidad del Condominio que el guardia Jorge ha sido despedido por no cumplir con sus labores correctamente. Gracias.';
        $announcement->title = 'Despido de Guardia';
        $announcement->date = '2018-09-21';
        $announcement->expirationDate = '2018-09-23';
        $announcement->categoryID=1;
        $announcement->userID=1;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='Se le informa a toda la comunidad del Condominio que el Administrador Juan ha sido despedido por ser un corrupto y ha sido inculpado de lavado de dinero. Gracias.';
        $announcement->title = 'Despido de Admin Juan';
        $announcement->date = '2018-09-21';
        $announcement->expirationDate = '2018-09-23';
        $announcement->categoryID=3;
        $announcement->userID=1;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='A toda la comunidad del coto neptuno se les informa que se hará cambio de puertas de la entrada sur, ya que están en mal estado.';
        $announcement->title = 'Cambio de Puertas';
        $announcement->date = '2018-09-24';
        $announcement->expirationDate = '2018-09-27';
        $announcement->categoryID=1;
        $announcement->userID=1;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='Se le pide a toda la comunidad de Neptuno que recuerden el reglamento intero donde a partir de las 11 p.m. no se debe poner música en niveles altos ya que alteran la tranquilidad del Condominio';
        $announcement->title = 'Música alta';
        $announcement->date = '2018-09-24';
        $announcement->expirationDate = '2018-09-27';
        $announcement->categoryID=8;
        $announcement->userID=1;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='Se le informa a toda la comunidad que la cuota mensual a pagar al Administrador ha aumentado en $50.00, es decir será de $350.00';
        $announcement->title = 'Aumento de Cuotas';
        $announcement->date = '2018-09-24';
        $announcement->expirationDate = '2018-09-27';
        $announcement->categoryID=1;
        $announcement->userID=2;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='Como Administrador de este Condominio me complace informarles que a partir de que contamos con el nuevo sistema la seguridad ha aumentado considerablemente';
        $announcement->title = 'Seguridad';
        $announcement->date = '2018-09-24';
        $announcement->expirationDate = '2018-09-27';
        $announcement->categoryID=3;
        $announcement->userID=2;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='Se le informa a la comunidad que el alumbrado del Condominio ha sido cambiado casi en su totalidad en el área sur.';
        $announcement->title = 'Alumbrado del área Sur';
        $announcement->date = '2018-09-29';
        $announcement->expirationDate = '2018-10-03';
        $announcement->categoryID=2;
        $announcement->userID=2;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='Este mes se les informa a los vecinos de las UP con número 7,10,34,58,63 y 112 que por favor pasen a pagar su cuota del mes para ponerse al corriente. Gracias.';
        $announcement->title = 'Pagos Pendientes';
        $announcement->date = '2018-09-29';
        $announcement->expirationDate = '2018-10-03';
        $announcement->categoryID=6;
        $announcement->userID=1;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='Se les informa a toda la comunidad que la mesa directiva del Condominio ha decidido invertir el dinero de la caja chica en mejoras al parque.';
        $announcement->title = 'Mejoras al Parque';
        $announcement->date = '2018-09-29';
        $announcement->expirationDate = '2018-10-03';
        $announcement->categoryID=5;
        $announcement->userID=1;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='A toda la comunidad se les solicita de favor tener cuidado de sus mascotas ya que algunos vecinos han reportado que ciertos dueños no recogen los desechos de sus mascotas.';
        $announcement->title = 'Cuidado de Mascotas';
        $announcement->date = '2018-09-29';
        $announcement->expirationDate = '2018-10-03';
        $announcement->categoryID=8;
        $announcement->userID=1;
        $announcement->save();

        $announcement = new Announcement();
        $announcement->content ='Es de mi gusto informarles que contamos con un nuevo guardia de seguridad que estará al servicio de todos los habitantes en la entrada norte del Condominio. Su nombre es Juan Ayala, denle un poco de tiempo para adaptarse al fraccionamiento.';
        $announcement->title = 'Nuevo Guardia';
        $announcement->date = '2018-09-29';
        $announcement->expirationDate = '2018-10-03';
        $announcement->categoryID=3;
        $announcement->userID=1;
        $announcement->save();

    }
}
