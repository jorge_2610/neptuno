<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->email = 'jorge@hotmail.com';
        $user->username = 'Jorge Contreras';
        $user->password = bcrypt('secret');
        $user->profileID = "17";
        $user->type = 'Admin';
        $user->save();

        $user = new User();
        $user->email = 'Juan@hotmail.com';
        $user->username = 'Haunter';
        $user->password = bcrypt('12345');
        $user->profileID = "12";
        $user->type = 'Condomino';
        $user->save();

        $user = new User();
        $user->email = 'Gris@hotmail.com';
        $user->username = 'Gris';
        $user->password = bcrypt('55555');
        $user->profileID = "5";
        $user->type = 'Condomino';
        $user->save();
    }
}
