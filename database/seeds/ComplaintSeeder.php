<?php

use App\Complaint;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComplaintSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $complaint = new Complaint();
        $complaint->content ='Este es una Queja de Prueba Nueva.';
        $complaint->title = 'Queja de Prueba';
        $complaint->date = '2018-10-20';
        $complaint->status = 'Nueva';
        $complaint->categoryID=1;
        $complaint->userID=3;
        $complaint->save();

        $complaint = new Complaint();
        $complaint->content ='Este es una Queja de Prueba Procesando.';
        $complaint->title = 'Queja de Prueba';
        $complaint->date = '2018-10-25';
        $complaint->status = 'Procesando';
        $complaint->categoryID=3;
        $complaint->userID=3;
        $complaint->save();

        $complaint = new Complaint();
        $complaint->content ='Este es una Queja de Prueba Resuelta.';
        $complaint->title = 'Queja de Prueba';
        $complaint->date = '2018-10-22';
        $complaint->status = 'Resuelta';
        $complaint->categoryID=6;
        $complaint->userID=3;
        $complaint->save();

        $complaint = new Complaint();
        $complaint->content ='Este es una Queja de Prueba Resuelta.';
        $complaint->title = 'Queja de Prueba';
        $complaint->date = '2018-10-22';
        $complaint->status = 'Nueva';
        $complaint->categoryID=6;
        $complaint->userID=3;
        $complaint->save();

        $complaint = new Complaint();
        $complaint->content ='Este es una Queja de Prueba Resuelta.';
        $complaint->title = 'Queja de Prueba';
        $complaint->date = '2018-10-22';
        $complaint->status = 'Resuelta';
        $complaint->categoryID=6;
        $complaint->userID=3;
        $complaint->save();

        $complaint = new Complaint();
        $complaint->content ='Este es una Queja de Prueba Resuelta.';
        $complaint->title = 'Queja de Prueba';
        $complaint->date = '2018-10-22';
        $complaint->status = 'Procesando';
        $complaint->categoryID=6;
        $complaint->userID=3;
        $complaint->save();
    }
}
