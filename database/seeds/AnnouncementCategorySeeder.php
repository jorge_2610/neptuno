<?php

use App\AnnouncementsCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class AnnouncementCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = new AnnouncementsCategory();
        $category->name = "Anuncios";
        $category->save();

        $category = new AnnouncementsCategory();
        $category->name = "Mantenimiento";
        $category->save();

        $category = new AnnouncementsCategory();
        $category->name = "Seguridad";
        $category->save();

        $category = new AnnouncementsCategory();
        $category->name = "Áreas Verdes";
        $category->save();

        $category = new AnnouncementsCategory();
        $category->name = "Gastos";
        $category->save();

        $category = new AnnouncementsCategory();
        $category->name = "Cuotas";
        $category->save();

        $category = new AnnouncementsCategory();
        $category->name = "Personal";
        $category->save();

        $category = new AnnouncementsCategory();
        $category->name = "Normas de Conviencia";
        $category->save();
    }
}
