<?php

use App\Comment;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $comment = new Comment();
        $comment->date = "2018-10-04";
        $comment->content = "Comentario de Prueba en el anuncio de prueba.";
        $comment->userID = 3;
        $comment->announcementID = 1;
        $comment->save();


        $comment = new Comment();
        $comment->date = "2018-10-04";
        $comment->content = "Me consta, dejaba pasar a cualquiera al fraccionamiento.";
        $comment->userID = 3;
        $comment->announcementID = 2;
        $comment->save();

        $comment = new Comment();
        $comment->date = "2018-10-05";
        $comment->content = "Eso no es cierto, a mi hermano siempre le pedía su identificación.";
        $comment->userID = 2;
        $comment->announcementID = 2;
        $comment->save();

        $comment = new Comment();
        $comment->date = "2018-10-05";
        $comment->content = "De lo que se viene a enterar uno, que mal por él, parecía buena persona.";
        $comment->userID = 3;
        $comment->announcementID = 3;
        $comment->save();

        $comment = new Comment();
        $comment->date = "2018-10-05";
        $comment->content = "Al fin las Cambiaron, era un poco molesto tener que bajarnos a abrir la puerta.";
        $comment->userID = 3;
        $comment->announcementID = 4;
        $comment->save();

        $comment = new Comment();
        $comment->date = "2018-10-05";
        $comment->content = "Muchas gracias.";
        $comment->userID = 2;
        $comment->announcementID = 4;
        $comment->save();

        $comment = new Comment();
        $comment->date = "2018-10-05";
        $comment->content = "Al contrario, gracias a ustedes por reportarlo.";
        $comment->userID = 2;
        $comment->announcementID = 4;
        $comment->save();
    }
}
