<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements_images', function (Blueprint $table) {
            $table->unsignedBigInteger("announcementID");
            $table->unsignedBigInteger("imageID");
            $table->foreign('announcementID')->references('id')->on('Announcements')->onUpdate('cascade');
            $table->foreign('imageID')->references('id')->on('Images')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements_images');
    }
}
