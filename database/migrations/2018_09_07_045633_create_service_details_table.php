<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_details', function (Blueprint $table) {
            $table->string('privativeUnit',100);
            $table->unsignedInteger("serviceID");
            $table->boolean('active');
            $table->foreign('privativeUnit')->references('id')->on('Privative_Units')->onUpdate('cascade');
            $table->foreign('serviceID')->references('id')->on('Services')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_details');
    }
}
