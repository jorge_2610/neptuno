<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_accesses', function (Blueprint $table) {
            $table->unsignedInteger('userID');
            $table->date("date");
            $table->time("hour");
            $table->foreign('userID')->references('id')->on('Users')->onUpdate('cascade');
            $table->primary(['userID', 'date', 'hour']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_accesses');
    }
}
