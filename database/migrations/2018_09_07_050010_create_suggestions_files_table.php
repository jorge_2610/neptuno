<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestionsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggestions_files', function (Blueprint $table) {
            $table->unsignedBigInteger("suggestionID");
            $table->unsignedBigInteger("fileID");
            $table->foreign('suggestionID')->references('id')->on('Suggestions')->onUpdate('cascade');
            $table->foreign('fileID')->references('id')->on('Files')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggestions_files');
    }
}
