<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints_images', function (Blueprint $table) {
            $table->unsignedBigInteger("complaintID");
            $table->unsignedBigInteger("imageID");
            $table->foreign('complaintID')->references('id')->on('Complaints')->onUpdate('cascade');
            $table->foreign('imageID')->references('id')->on('Images')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints_images');
    }
}
