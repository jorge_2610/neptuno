<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEditedExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edited_expenses', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->double('originalAmount');
            $table->double('updatedAmount');
            $table->dateTime("date");
            $table->unsignedInteger("administratorID");
            $table->unsignedBigInteger("expenseID");
            $table->foreign('administratorID')->references('id')->on('Users')->onUpdate('cascade');
            $table->foreign('expenseID')->references('id')->on('Expenses')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edited_expenses');
    }
}
