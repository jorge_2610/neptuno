<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHabitantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('habitants', function (Blueprint $table) {
            $table->string("INE",100)->unique();
            $table->string('name');
            $table->string('email');
            $table->bigInteger("phone");
            $table->bigInteger("cellphone");
            $table->string('city');
            $table->string('state');
            $table->string('address');
            $table->text('observations');
            $table->string('bankReference');
            $table->date("startDate");
            $table->date("finishDate");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('habitants');
    }
}
