<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incomes', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string('privativeUnit',100);
            $table->double("amount");
            $table->date("date");
            $table->unsignedBigInteger("imageID");
            $table->text('comments');
            $table->string('paymentMethod');
            $table->unsignedInteger("accountID");
            $table->unsignedInteger("administratorID");
            $table->foreign('privativeUnit')->references('id')->on('Privative_Units')->onUpdate('cascade');
            $table->foreign('imageID')->references('id')->on('Images')->onUpdate('cascade');
            $table->foreign('accountID')->references('id')->on('Banks')->onUpdate('cascade');
            $table->foreign('administratorID')->references('id')->on('Users')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incomes');
    }
}
