<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCondonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condonations', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->double("amount");
            $table->date("date");
            $table->unsignedBigInteger("feeID");
            $table->foreign('feeID')->references('id')->on('Fees')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condonations');
    }
}
