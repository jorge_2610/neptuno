<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string('privativeUnit',100);
            $table->string('concept');
            $table->string('provider');
            $table->double("amount");
            $table->unsignedInteger("accountID");
            $table->date("date");
            $table->unsignedBigInteger("imageID");
            $table->unsignedInteger('categoryID');
            $table->text('comments');
            $table->string('paymentMethod');
            $table->unsignedInteger("administratorID");
            $table->foreign('privativeUnit')->references('id')->on('Privative_Units')->onUpdate('cascade');
            $table->foreign('accountID')->references('id')->on('Banks')->onUpdate('cascade');
            $table->foreign('imageID')->references('id')->on('Images')->onUpdate('cascade');
            $table->foreign('categoryID')->references('id')->on('Expenses_Categories')->onUpdate('cascade');
            $table->foreign('administratorID')->references('id')->on('Users')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
