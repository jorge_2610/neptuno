<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivativeUnitAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privative_unit_accesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime("entryDate");
            $table->dateTime("departureDate");
            $table->string("destinationPU",100);
            $table->string("personID",100);
            $table->integer("peopleNumber");
            $table->unsignedInteger('categoryID');
            $table->string('gaffete');
            $table->foreign('destinationPU')->references('id')->on('Privative_Units')->onUpdate('cascade');
            $table->foreign('categoryID')->references('id')->on('Access_Categories')->onUpdate('cascade');
            $table->foreign('personID')->references('INE')->on('Person_Accesses')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('privative_unit_accesses');
    }
}
