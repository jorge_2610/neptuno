<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('content');
            $table->dateTime("date");
            $table->string('status');
            $table->unsignedInteger('categoryID');
            $table->text('consecutive')->nullable();
            $table->unsignedInteger('userID');
            $table->foreign('categoryID')->references('id')->on('Announcements_Categories')->onUpdate('cascade');
            $table->foreign('userID')->references('id')->on('Users')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints');
    }
}
