<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('complaints_files', function (Blueprint $table) {
            $table->unsignedBigInteger("complaintID");
            $table->unsignedBigInteger("fileID");
            $table->foreign('complaintID')->references('id')->on('Complaints')->onUpdate('cascade');
            $table->foreign('fileID')->references('id')->on('Files')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('complaints_files');
    }
}
