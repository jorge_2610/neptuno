<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuggestionsImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggestions_images', function (Blueprint $table) {
            $table->unsignedBigInteger("suggestionID");
            $table->unsignedBigInteger("imageID");
            $table->foreign('suggestionID')->references('id')->on('Suggestions')->onUpdate('cascade');
            $table->foreign('imageID')->references('id')->on('Images')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggestions_images');
    }
}
