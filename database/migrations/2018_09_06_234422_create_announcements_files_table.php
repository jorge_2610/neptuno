<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements_files', function (Blueprint $table) {
            $table->unsignedBigInteger("announcementID");
            $table->unsignedBigInteger("fileID");
            $table->foreign('announcementID')->references('id')->on('Announcements')->onUpdate('cascade');
            $table->foreign('fileID')->references('id')->on('Files')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements_files');
    }
}
