<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePositiveBalancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positive_balances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double("amount");
            $table->unsignedBigInteger("incomeID");
            $table->string('privativeUnit',100);
            $table->unsignedInteger('categoryID');
            $table->foreign('privativeUnit')->references('id')->on('Privative_Units')->onUpdate('cascade');
            $table->foreign('incomeID')->references('id')->on('Incomes')->onUpdate('cascade');
            $table->foreign('categoryID')->references('id')->on('Expenses_Categories')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positive_balances');
    }
}
