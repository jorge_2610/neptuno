<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime("date");
            $table->text('content');
            $table->unsignedInteger('userID');
            $table->unsignedBigInteger('announcementID');
            $table->foreign('userID')->references('id')->on('Users')->onUpdate('cascade');
            $table->foreign('announcementID')->references('id')->on('Announcements')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
