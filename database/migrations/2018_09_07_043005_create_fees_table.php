<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->string('privativeUnit',100);
            $table->double("amount");
            $table->date("deadLine");
            $table->boolean('paidOut');
            $table->unsignedBigInteger("incomeID");
            $table->unsignedInteger('categoryID');
            $table->foreign('privativeUnit')->references('id')->on('Privative_Units')->onUpdate('cascade');
            $table->foreign('categoryID')->references('id')->on('Expenses_Categories')->onUpdate('cascade');
            $table->foreign('incomeID')->references('id')->on('Incomes')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees');
    }
}
